package mineSweeper;

public class Cell {
  private final int xCoordinate,yCoordinate;
  private final CellState state;

  public Cell(int xCoordinate, int yCoordinate, CellState state) {
    this.xCoordinate = xCoordinate;
    this.yCoordinate = yCoordinate;
    this.state = state;
  }

  public boolean isAMine(){
    return this.state==CellState.MINE;
  }

  public boolean cellWithXY(int xy[]){
    return xy[0]==xCoordinate && xy[1]==yCoordinate;
  }
}
