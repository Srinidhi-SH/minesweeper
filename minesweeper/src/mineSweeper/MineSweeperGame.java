package mineSweeper;

import java.util.Scanner;

import static mineSweeper.MineSweeper.*;

public class MineSweeperGame {

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    MineSweeper mineSweeper = inputAndPlayMineSweeper(scanner.nextLine());
    int numberOfGuess = 0;

    while (numberOfGuess < 6) {
      String message = mineSweeper.tryAGuess(scanner.nextLine());
      System.out.println(message);
      if (message.equals("Oops, you stepped on a mine ! Game over !")) System.exit(0);
      numberOfGuess++;
    }
    if (numberOfGuess == 6) System.out.println("Successfully DE-MINED The field");
  }
}
