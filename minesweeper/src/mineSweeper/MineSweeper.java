package mineSweeper;


import java.util.ArrayList;

import static java.lang.Integer.*;
import static mineSweeper.CellState.*;

public class MineSweeper {

  private final ArrayList<Cell> cellsInField;
  private final int sizeOfField;

  public MineSweeper(ArrayList<Cell> cellsInField) {
    this.cellsInField = cellsInField;
    sizeOfField=cellsInField.size()/3;
  }

  public static MineSweeper inputAndPlayMineSweeper(String cells){
    ArrayList<Cell> cellsInField = new ArrayList<Cell>();
    String rows[]=cells.split(",");
    for (int row=0;row<rows.length;row++)
      for (int column = 0; column < rows[0].length(); column++) {
        if (rows[row].charAt(column) == 'm') {
          cellsInField.add(new Cell(row, column, MINE));
          continue;
        }
        cellsInField.add(new Cell(row, column, LAND));
      }
    return new MineSweeper(cellsInField);
  }

  public String tryAGuess(String guess){
    int xy[]= new int[]{parseInt(guess.split(",")[0]), parseInt(guess.split(",")[1])};
    if (isCellWithCoordinatesAMine(xy)==1)
      return "Oops, you stepped on a mine ! Game over !";
    return String.valueOf(numberOfMinesAroundThis(xy));
  }

  public int numberOfMinesAroundThis(int[] xy) {
    int number=0;
    if (xy[0] - 1 >= 0) number+= isCellWithCoordinatesAMine(new int[]{xy[0] - 1, xy[1]});
    if (xy[1] - 1 >= 0) number+= isCellWithCoordinatesAMine(new int[]{xy[0], xy[1] - 1});
    if (xy[1] + 1 <= sizeOfField - 1) number+= isCellWithCoordinatesAMine(new int[]{xy[0], xy[1] + 1});
    if (xy[0] + 1 <= sizeOfField - 1) number+= isCellWithCoordinatesAMine(new int[]{xy[0] + 1, xy[1]});
    return number;
  }

  public int isCellWithCoordinatesAMine(int[] xy) {
    return cellWithCoordinates(xy).isAMine()?1:0;
  }

  private Cell cellWithCoordinates(int [] xy){
    for (Cell eachCell:cellsInField)
      if (eachCell.cellWithXY(xy))
        return eachCell;
    return null;
  }
}
