package mineSweeper;

import org.junit.Assert;
import org.junit.Test;

public class MineSweeperTest {

  @Test
  public void numberOfMinesAroundASetOfCoordinates(){
    Assert.assertEquals(2,getMineSweeper().numberOfMinesAroundThis(new int[]{0, 1}));
  }

  @Test
  public void cellWithGivenCoordinatesAMine(){
    Assert.assertEquals(1,getMineSweeper().isCellWithCoordinatesAMine(new int[]{0, 0}));
  }

  @Test
  public void tryAGuessForBombing(){
    Assert.assertEquals("Oops, you stepped on a mine ! Game over !",getMineSweeper().tryAGuess("1,1"));
  }

  @Test
  public void tryAGuessForAnotherBombing(){
    Assert.assertEquals("Oops, you stepped on a mine ! Game over !",getMineSweeper().tryAGuess("2,2"));
  }

  @Test // 0,1 -> 2
  public void tryAGuessForANumber(){
    Assert.assertEquals("2",getMineSweeper().tryAGuess("0,1"));
  }

  @Test // 0,2 -> 0
  public void tryAGuessForAnotherNumber(){
    Assert.assertEquals("0",getMineSweeper().tryAGuess("0,2"));
  }

  private MineSweeper getMineSweeper(){
    return MineSweeper.inputAndPlayMineSweeper("mxx,xmx,xxm");
  }
}
