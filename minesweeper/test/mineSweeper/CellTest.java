package mineSweeper;

import org.junit.Assert;
import org.junit.Test;

import static mineSweeper.CellState.*;

public class CellTest {

  @Test
  public void checkACellToBeAMine(){
    Assert.assertTrue(new Cell(1,1, MINE).isAMine());
  }

  @Test
  public void isThisACellWithGivenCoordinates(){
    Assert.assertTrue(new Cell(1,1, MINE).cellWithXY(new int[]{1, 1}));
  }
}
